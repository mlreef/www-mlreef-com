![Build Status](https://gitlab.com/mlreef/www-mlreef-com/badges/master/build.svg)
# www-mlreef-com

---
This repository contains the source code of the official **MLReef** homepage https://about.mlreef.com

As part of our value of being transparent we welcome feedback. Please make a
[merge request](https://gitlab.com/mlreef/www-mlreef-com/merge_requests) to
suggest improvements or add clarifications. Please use
[issues](https://gitlab.com/mlreef/www-mlreef-com/issues) to ask questions.

---

## Getting started

- Clone the repository
- `bin/npm install` to install all dependencies
- `bin/npm start` to run the development server in `localhost:8000`
  + GraphQL interactive in `localhost:8000/__graphql`

- `bin/npm run build:serve` to build production and run a static server in `localhost:9000`

No *.env* file is required.

---

## [MLReef Handbook](handbook/README.md)

## [Developer Guidelines](developers.md)
