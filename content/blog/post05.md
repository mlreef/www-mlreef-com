---
title: What's important in data science?
author: Camillo Pachmann
date: 2020-10-21
image: '/images/blog/Survey_Results.png'
---

The field of data science has acquired an enormous attention in the past few years. Many new tools and services are appearing that offer data scientists and ML practitioners new ways of transforming their way of work.
As part of our own product development effort, we wanted to know understand what the general important topics are and which matter the most. We conducted an online survey to find out that out. Here are the results:

The survey was conducted online and anonymous via Typeform (participate in the survey [here](https://mlreef.typeform.com/to/w4UIkT)). In total 103 participants (=N) have completed the survey. This number is, by no means, fully representative but it gives a good hint. We summarized some of the qualitative questions in the survey to make the result more readable.

Hope you enjoy, we definitely did!

Please feel free to join the survey — we will continuously provide updates.



**Q: How many years of experience do you have in ML + Q: On what use case are you working on?**

<img src="https://miro.medium.com/max/700/1*1Ik7fZfKGJTxlwj5RYqHDQ.png"
     alt="Experience"
     style="margin-right: 10px;" />




**Q: What do you find most frustration during ML development?**

<img src="https://miro.medium.com/max/700/1*oObGuiPgMqzZXFxC1Y8W4w.png"
     alt="Pains"
     style="margin-right: 10px;" />




**Q: How important is version control on data for your work?**

<img src="https://miro.medium.com/max/700/1*6mpsqKYU_M9bc3-LcUJ6tA.gif"
     alt="VC Data"
     style="margin-right: 10px;" />

> Noticeable correlation: the more the use case is for “production”, the higher the importance for version control on data.



**Q: How important is version control on models for your work?**

<img src="https://miro.medium.com/max/700/1*bQuehNPyOMx6Wf-x54jNCw.png"
     alt="VC Models"
     style="margin-right: 10px;" />

> Noticeable correlation: it seems that only for “experimental” use cases or for inexperienced data scientist version control on models is not so important.



**Q: How important is community provided content (such as data sets, data operations) for your work?**

<img src="https://miro.medium.com/max/700/1*Q8WDr0dMju6JjyIPQpckWA.png"
     alt="Community content"
     style="margin-right: 10px;" />

> Noticeable correlation: it seems that only for experienced data scientists (above 10 years of experience) the importance of open source content diminishes. For inexperienced data scientists it is very important.



**Q: How important is replicability of results for your work?**

<img src="https://miro.medium.com/max/700/1*e9NFQOz7YLsZNbwllaN0IA.png"
     alt="Replicability"
     style="margin-right: 10px;" />

> Noticeable correlation: no matter on what use cases or how experienced, replicability is always very important for the majority of participants.



**Q: What tools do you use for ML development?**

<img src="https://miro.medium.com/max/700/1*Beim3dwjcBck6ZkJBqJFRQ.png"
     alt="ML tools"
     style="margin-right: 10px;" />





**Q: What takes the most time of your work?**

<img src="https://miro.medium.com/max/700/1*IkgBjgNya5p3gMQP2tXYYQ.png"
     alt="ML Time"
     style="margin-right: 10px;" />




Please feel free to reach out for any comments!
