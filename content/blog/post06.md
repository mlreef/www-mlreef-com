---
title: Useful resources to propel your Machine learning ship
author: Erika Torres
date: 2020-11-04
image: '/images/blog/space.jpeg'
---

I have been navigating through the Machine learning space for 8 years, sometimes it can be an overwhelming task, you need to read a lot of papers, learn programming languages and implement several strategies in a very short time. In this post I share some of my favorite resources that will make your journey more enjoyable.

From my personal experience I can say that I could not have made it so far without the collaboration of all my fellow travelers. Fortunately, the ML community have developed very useful guides to learn new techniques in less time. I am very grateful with them for being so active and supportive.

In the old days we had only GitHub and tons of papers to help us with demo building, but trying to reproduce an academic paper can take a lot of time and energy. Nowadays everything changed and we should be thankful for that. We have at our disposal a wide variety of sites with already trained models. You can even run train models with GPU without expending one cent, with services like [Azure](https://azure.microsoft.com/en-us/offers/ms-azr-0044p/) and [Google colab](https://colab.research.google.com/notebooks/intro.ipynb#recent=true).

Nowadays, we have access to all the best machine learning algorithms packed and ready for you to play with. Some popular models are included in Python packages such as PyTorch and TensorFlow. Although I want to talk about sites with contents that will help you understand and learn too. Without further ado, I would like to share with you some of the best resources that I have found during this trip.

For a beautiful ML algorithms collection [modelzoo](https://modelzoo.co/): You can submerge in hundreds of papers with images and videos, making it very didactic, some of them provide a downloadable Docker image, so you can explore comfortably at home.

<img src="https://miro.medium.com/max/875/1*T9B96ZIcDGN-eHcTdMxlow.png"
     alt="Experience"
     style="margin-right: 10px;" />


In [wandb.ai](https://wandb.ai/gallery) you can find stunning plots and details of all the experiments and results from hundreds of papers, it is a very pleasant site where you can spend hours reading and learning. One of my favorites!

<img src="https://miro.medium.com/max/875/1*nNgAS-qsQUw1bkEPA_9s2g.png"
     alt="Experience"
     style="margin-right: 10px;" />


With [TensorFlowHub](https://tfhub.dev/s?subtype=model-family) you have hundreds of examples to get your ship going, this platform offers practical code, way more serious but very useful.

<img src="https://miro.medium.com/max/875/1*q-s2hmRnRy3zQTjaRrkiWQ.png"
     alt="Experience"
     style="margin-right: 10px;" />


Conclusion:

I hope that you find these resources useful and that at least one those sites had enticed your curiosity. Sometimes we need a little boost when we feel unmotivated.

Thank you for reading!