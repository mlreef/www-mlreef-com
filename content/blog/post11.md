---
title: 'The 2022 Market Guide Reports for Multipersona and Engineering Data Science and ML Platforms'
author: Camillo Pachmann
date: 2022-05-23
image: '/images/blog/gartner.jpg'
---
# Gartner is fully in-line with MLReef´s value proposition

## Empowering more people across the organization

MLReef is proud to be the first platform to introduce Distributed Machine Learning (ML) development. The base principle is to open the development process to different stakeholders across the organization via a collaborative and structured development platform. Now, Gartner has released a new study showing the necessities for a multipersona development approach to harness the full power of Machine Learning. 

<img src="/images/blog/multipersona_dsml.png"
     alt="MLReef - the first platform for distributed ML development"
     style="margin-right: 20px;" />

According to Gartner, “The primary aim of multipersona DSML platforms is to create value through democratization. This is achieved by bringing the power of DSML to a wider technical and nontechnical audience while hiding complexity under the hood by automation and augmentation throughout all phases in the DSML development and operationalization process.”

### Gartner´s recommendation

Data & analytics leaders responsible for data science and machine learning should:

•	Democratize the use of DSML by business personas and other nonexpert data science personas by introducing multipersona DSML platforms in lines of business or across the enterprise and ecosystem, enabling the sharing of insights and experience.

•	Start offering support for decision intelligence by selecting multipersona DSML platforms that include capabilities for the full spectrum of descriptive, diagnostic, predictive and prescriptive analytics along with composability to allow adaptive assembly of new solutions.

•	Improve business value creation through DSML by using multipersona DSML platforms to move models beyond prototyping toward production, enabled by operationalization capabilities of these platforms and by fostering collaboration between citizen and expert data scientists as well as other personas.

•	Enable the ethical use of AI, including compliance with regulations, privacy protection and bias mitigation, by leveraging governance, risk management and responsible AI capabilities of multipersona DSML platforms.

The key highlight, according to the study, is "Multipersona DSML platforms offer a **shorter time to value** by empowering more people across the organization through ease-of-use and the augmentation or automation of activities as much as possible. **Ideally this is achieved without sacrificing flexibility**, especially for a more technical audience, needed to create solutions that require extensive tailoring. Similarly, multipersona DSML platforms should not be limited to model prototyping and development, but instead should fully support deploying, operationalizing and monitoring of DSML models. This should not only cover technical aspects but also governance, risk management and responsible AI practices."

**This stands in-line with MLReef´s value proposition and highlights our aim at democratizing ML development via distributed Machine Learning development.**

--- 

*Disclaimer*

*Gartner, Inc., Market Guide for DSML Engineering Platforms, Afraz Jaffri, Erick Brethenoux, et al. May 2022. Market Guide for Multipersona Data Science and Machine Learning Platforms, Pieter den Hamer, Carlie Idoine, et al. May 2022. Gartner does not endorse any vendor, product or service depicted in its research publications, and does not advise technology users to select only those vendors with the highest ratings or other designation. Gartner research publications consist of the opinions of Gartner’s research organization and should not be construed as statements of fact. Gartner disclaims all warranties, expressed or implied, with respect to this research, including any warranties of merchantability or fitness for a particular purpose. © [2022] Gartner, Inc. and/or its affiliates. All rights reserved. This graphic was published by Gartner, Inc. as part of a larger research document and should be evaluated in the context of the entire document. The Gartner document is available upon request.*