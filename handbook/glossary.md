MLReef#s Glossar
====================

* **Admin Token:** The Gitlab API Admin token that is created during the installation of MLReef. Lifetime: indefinite
* **At arms length:** At a distance, avoiding intimacy or familiarity. Meaning e.g. for a ticket description to include certain details, 
  because even if the intended developer would know them, others might not.
* **Business Requirmentes:** see Requirements
* **Namespace:** The name slug of the Subject who is the owner of a project; Either the project owner's username slug or the group name slug)
* **OAUTH-token:** The temporary token Gitlab creates for us during a User's login. Lifetime: 2 hours
* **Project-slug**: Slug of the project's name
* **Requirements:** Stating the desired state with as **little** technical details as possible.
  Technical details should only be added if they are in fact part of the User's expectation.
  See also: Technical Specification
* **Slug:** a simplified version of a String containing only lowercase characters and with everything except 0-9 and a-z replaced with -. Use in URLs and domain names.
* **Specification:** see Technical Specification
* **Technical Specification:** A technical description of the implementation which can fulfil a Requirements. 