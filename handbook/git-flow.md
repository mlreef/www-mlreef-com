Introduction to GitLab Flow
==============================
original: [docs.gitlab.com/ee/workflow/gitlab_flow.html](https://docs.gitlab.com/ee/workflow/gitlab_flow.html)

Adapted to better fit the MLReef project.

---

![GitLab Flow](img/gitlab_flow.png)

Git allows a wide variety of branching strategies and workflows.
Because of this, many organizations end up with workflows that are too complicated, not clearly defined, or not integrated with issue tracking systems.
Therefore, Gitlab has proposed GitLab flow as a clearly defined set of best practices.
It combines [feature-driven development](https://en.wikipedia.org/wiki/Feature-driven_development) and 
[feature branches](https://martinfowler.com/bliki/FeatureBranch.html) with issue tracking.


Branches and Usage
--------------------

At MLReef we have two distinct requirements in regards to deployment environments:

* Have a stabled, tested, working state of the project for demos, investor meetings and future usage
* Support fast iterations of coding, adding features or refactorings

Therefore, we decided to use two different environment branches.


### Branch "develop"

* the default branch for working, branching away, and merge into (via Merge Requests)
* branch from develop, start a "feature/*" branch, merge into it
* keep the develop branch stable and working!
* develop will be tested periodically and will be merged into master via fast forward merges
* protected, and not to be used directly. Work happens in feature and hotfix branches


### Branch "master"

* default branch to get current, stable and tested state of projects for actual usage
* protected, and not advised to used be generally. Merge Requests into master are only allowed from develop, and executed by Maintainers.
* has the meaning "We have a stable version, which represents the best state we have at the moment and want to show"


Hint: The following document is a default git-flow of gitlab, in images "master" is shown, but we mean "develop" branch as "default for working", and "master" as "default for usage"


Issue tracking
------------------------------

![Merge request with the branch name "15-require-a-password-to-change-it" and assignee field shown](img/merge_request.png)

Any significant change to the code should start with an issue that describes the goal. Having a reason for every code change helps to inform the rest of the team and to keep the scope of a feature branch small.

In MLReef, each change to the codebase starts with an issue in the issue tracking system.
Raising an issue is part of the development process because they are used in sprint planning. 
If there is no issue yet, and the change will take a significant amount of work, -i.e.  more than 1 hour - create the issue.

**The issue title should describe the desired state of the system.**

When you are ready to code, create a branch for the issue from the `develop` branch.
This branch is the place for any work related to this change.

When you are done or want to discuss the code, open a [merge request](Merge requests with GitLab).
A merge request is an online place to discuss the change and review the code.

Make sure to update the ticket and/or the merge request descriptions with new information that is discovered during the process.

After the merge, delete the feature branch since it is no longer needed.
In GitLab, this deletion is an option when merging.

Suppose that a branch is merged but a problem occurs and the issue is reopened.
In this case, it is no problem to reuse the same branch name since the first branch was deleted when it was merged.
At any time, there is at most one branch for every issue.


### Issue States
- Open: this issue is ready for development
  * If the task does not have an assignee, everyone is free to take it and work on it
* If the task is assigned to a person, it is reserved for that person
  
- In Progress: This task is being worked on by developer it is assigned.
  * The developer is in the lead and it is their responsibility to push the completion of the ticket
  * If a Merge Request exists and the change is under review, the ticket stays assigned to the developer. Even when the 
  merge request is assigned to a different person, the developer stays responsible for the task.

- Closed: This issue is done and needs no more attention

### Issue template
There are different issue templates for use cases. 

**feature**: Any change that will add or change functionality or the user experience

**bug**: Any change that changes an error or another unwanted behaviour. 

Environment branches
------------------------------

![Multiple branches with the code cascading from one to another](img/environment_branches.png)

We have a testing environment, a pre-production environment, and a production environment.
The `master` branch is automatically deployed to the test environment.

To deploy to staging, create a merge request from the `master` branch to the production branch.

### Cherry Picking a commit

If you need to cherry-pick a commit with a hotfix, it is common to develop it on a feature branch and merge it into `develop`
with a merge request. In this case, do not delete the feature branch yet.
If `develop` passes automatic testing, you then merge the feature branch into the other branches.
If this is not possible because more manual testing is required, you can send merge requests from the feature branch to the downstream branches.



Merge requests with GitLab
------------------------------

![Merge request with inline comments](img/mr_inline_comments.png)

Merge requests are created in Gitlab. They ask an assigned person to merge two branches.

If you work on a feature branch for more than a few hours, it is good to share the intermediate result with the rest of the team.
To do this, create a merge request without assigning it to anyone.
Instead, mention people in the description or a comment, for example, "/cc @mark @susan."
This indicates that the merge request is not ready to be merged yet, but feedback is welcome.
Your team members can comment on the merge request in general or on specific lines with line comments.
The merge request serves as a code review tool, and no separate code review tools should be needed.
If the review reveals shortcomings, anyone can commit and push a fix.
Usually, the person to do this is the creator of the merge request.
The diff in the merge request automatically updates when new commits are pushed to the branch.

If you open the merge request but do not assign it to anyone, it is a "Work In Progress" merge request.
These are used to discuss the proposed implementation but are not ready for inclusion in the `master` branch yet.
Start the title of the merge request with `[WIP]` or `WIP:` to prevent it from being merged before it's ready.

When you are ready for your feature branch to be merged, assign the merge request to the person who knows most about the codebase you are changing.
Also, mention any other people from whom you would like feedback.
After the assigned person feels comfortable with the result, they can merge the branch by pressing the `merge` button.
When they press the merge button, GitLab merges the code and creates a merge commit that makes this event easily visible later on.

If the assigned person does not feel comfortable, they can request more changes or close the merge request without merging.

In GitLab, it is common to protect the long-lived branches, e.g., the `develop` branch, so that [most developers can't modify them](../permissions/permissions.md).
So, if you want to merge into a protected branch, assign your merge request to someone with maintainer permissions.


After you merge a feature branch, you should delete it. In GitLab, you can do this by checking the `Delete source branch` checkbox
during creation of the Merge Request and during the actual merging. Removing finished branches ensures that the list of 
branches shows only work in progress.

![Remove checkbox for branch in merge requests](img/remove_checkbox.png)

Suppose that a branch is merged but a problem occurs and the issue is reopened.
In this case, it is no problem to reuse the same branch name since the first branch was deleted when it was merged.
At any time, there is at most one branch for every issue.
It is possible that one feature branch solves more than one issue.

NOTE: **Note:**
* When you reopen an issue you need to create a new merge request.
* It is possible that one feature branch solves more than one issue.

### Requesting Changes to a Merge Request

As mentioned above, during the review process the assigned reviewer can request changes to the merge request. Depending
on the size and scope of the changes the Reviewer might assign the Merge Request back to the author. This way it is clear
who is currently in charge and whose responsibility it is to advance the Merge Request.

After you made the changes requested by the reviewer, assign the Merge Request to the reviewer again. This makes it clear
to the reviewer that the branch now can be merged and that he should do so if he is satisfied with the changes.

### Standard Method for Merge Requests

While creating a merge request it is a very common approach to squash all the commits leading up to a particular branch and deleting the source branch in the process.

Squashing lets you tidy up the commit history of a branch when accepting a merge request. It applies all of the changes in the merge request as a single commit, and then merges that commit using the merge method set for the project.

![Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/img/squash_mr_message.png)

Release branches
------------------------------
![Master and multiple release branches that vary in length with cherry-picks from master](img/release_branches.png)

We use release branches to package and deliver the MLReef Community- and Enterprise- Edition to the outside world.

Each branch contains a minor version, for example, 2-3-stable, 2-4-stable, etc.
Create stable branches using `master` as a starting point, and branch as late as possible.
By doing this, you minimize the length of time during which you have to apply bug fixes to multiple branches.
After announcing a release branch, only add serious bug fixes to the branch.
If possible, first merge these bug fixes into `master`, and then cherry-pick them into the release branch.
If you start by merging into the release branch, you might forget to cherry-pick them into `master`, and then you'd encounter the same bug in subsequent releases.
Merging into `master` and then cherry-picking into release is called an "upstream first" policy, which is also practiced by [Google](https://www.chromium.org/chromium-os/chromiumos-design-docs/upstream-first) and [Red Hat](https://www.redhat.com/en/blog/a-community-for-using-openstack-with-red-hat-rdo).
Every time you include a bug fix in a release branch, increase the patch version (to comply with [Semantic Versioning](https://semver.org/)) by setting a new tag.
Some projects also have a stable branch that points to the same commit as the latest released branch.
In this flow, it is not common to have a production branch (or Git flow `master` branch).


Linking and closing issues from merge requests
------------------------------
![Merge request showing the linked issues that will be closed](img/close_issue_mr.png)

Link to issues by mentioning them in commit messages or the description of a merge request, for example, "Fixes #16" or "Duck typing is preferred. See #12."
GitLab then creates links to the mentioned issues and creates comments in the issues linking back to the merge request.

To automatically close linked issues, mention them with the words "fixes" or "closes," for example, "fixes #14" or "closes #67." GitLab closes these issues when the code is merged into the default branch.

If you have an issue that spans across multiple repositories, create an issue for each repository and link all issues to a parent issue.


Squashing commits with rebase
------------------------------
![Vim screen showing the rebase view](img/rebase.png)

With Git, you can use an interactive rebase (`rebase -i`) to squash multiple commits into one or reorder them.
This functionality is useful if you want to replace a couple of small commits with a single commit, or if you want to make the order more logical.

However, you should never rebase commits you have pushed to a remote server.
Rebasing creates new commits for all your changes, which can cause confusion because the same change would have multiple identifiers.
It also causes merge errors for anyone working on the same branch because their history would not match with yours.
Also, if someone has already reviewed your code, rebasing makes it hard to tell what changed since the last review.

You should also never rebase commits authored by other people.
Not only does this rewrite history, but it also loses authorship information.
Rebasing prevents the other authors from being attributed and sharing part of the [`git blame`](https://git-scm.com/docs/git-blame).

If a merge involves many commits, it may seem more difficult to undo.
You might think to solve this by squashing all the changes into one commit before merging, but as discussed earlier, it is a bad idea to rebase commits that you have already pushed.
Fortunately, there is an easy way to undo a merge with all its commits.
The way to do this is by reverting the merge commit.
Preserving this ability to revert a merge is a good reason to always use the "no fast-forward" (`--no-ff`) strategy when you merge manually.

NOTE: **Note:**
If you revert a merge commit and then change your mind, revert the revert commit to redo the merge.
Git does not allow you to merge the code again otherwise.


Reducing merge commits in feature branches
------------------------------
![List of sequential merge commits](img/merge_commits.png)

Having lots of merge commits can make your repository history messy.
Therefore, you should try to avoid merge commits in feature branches.
Often, people avoid merge commits by just using rebase to reorder their commits after the commits on the `develop` branch.
Using rebase prevents a merge commit when merging `develop` into your feature branch, and it creates a neat linear history.
However, as discussed in [the section about rebasing](#squashing-commits-with-rebase), you should never rebase commits you have pushed to a remote server.
This restriction makes it impossible to rebase work in progress that you already shared with your team, which is something we recommend.

Rebasing also creates more work, since every time you rebase, you have to resolve similar conflicts.
Sometimes you can reuse recorded resolutions (`rerere`), but merging is better since you only have to resolve conflicts once.
Atlassian has a more thorough explanation of the tradeoffs between merging and rebasing [on their blog](https://www.atlassian.com/blog/git/git-team-workflows-merge-or-rebase).

A good way to prevent creating many merge commits is to not frequently merge `develop` into the feature branch.
There are three reasons to merge in `develop`: utilizing new code, resolving merge conflicts, and updating long-running branches.

If you need to utilize some code that was introduced in `develop` after you created the feature branch, you can often solve this by just cherry-picking a commit.

If your feature branch has a merge conflict, creating a merge commit is a standard way of solving this.

NOTE: **Note:**
Sometimes you can use .gitattributes to reduce merge conflicts.
For example, you can set your changelog file to use the [union merge driver](https://git-scm.com/docs/gitattributes#gitattributes-union) so that multiple new entries don't conflict with each other.

The last reason for creating merge commits is to keep long-running feature branches up-to-date with the latest state of the project.
The solution here is to keep your feature branches short-lived.
Most feature branches should take less than one day of work.
If your feature branches often take more than a day of work, try to split your features into smaller units of work.

If you need to keep a feature branch open for more than a day, there are a few strategies to keep it up-to-date.
One option is to use continuous integration (CI) to merge in `develop` at the start of the day.
Another option is to only merge in from well-defined points in time, for example, a tagged release.
You could also use [feature toggles](https://martinfowler.com/bliki/FeatureToggle.html) to hide incomplete features so you can still merge back into `develop` every day.

> **Note:** Don't confuse automatic branch testing with continuous integration.
> Martin Fowler makes this distinction in [his article about feature branches](https://martinfowler.com/bliki/FeatureBranch.html):
>
> "I've heard people say they are doing CI because they are running builds, perhaps using a CI server, on every branch with every commit.
> That's continuous building, and a Good Thing, but there's no *integration*, so it's not CI."

In conclusion, you should try to prevent merge commits, but not eliminate them.
Your codebase should be clean, but your history should represent what actually happened.
Developing software happens in small, messy steps, and it is OK to have your history reflect this.
You can use tools to view the network graphs of commits and understand the messy history that created your code.
If you rebase code, the history is incorrect, and there is no way for tools to remedy this because they can't deal with changing commit identifiers.


Commit often and push frequently
------------------------------

Another way to make your development work easier is to commit often.
Every time you have a working set of tests and code, you should make a commit.
Splitting up work into individual commits provides context for developers looking at your code later.
Smaller commits make it clear how a feature was developed, and they make it easy to roll back to a specific good point in time or to revert one code change without reverting several unrelated changes.

Committing often also makes it easy to share your work, which is important so that everyone is aware of what you are working on.
You should push your feature branch frequently, even when it is not yet ready for review.
By sharing your work in a feature branch or [a merge request](#mergepull-requests-with-gitlab-flow), you prevent your team members from duplicating work.
Sharing your work before it's complete also allows for discussion and feedback about the changes, which can help improve the code before it gets to review.



Working with feature branches
------------------------------

![Shell output showing git pull output](img/git_pull.png)

When creating a feature branch, always branch from an up-to-date `master`.
If you know before you start that your work depends on another branch, you can also branch from there.
If you need to merge in another branch after starting, explain the reason in the merge commit.
If you have not pushed your commits to a shared location yet, you can also incorporate changes by rebasing on `master` or another feature branch.
Do not merge from upstream again if your code can work and merge cleanly without doing so.
Merging only when needed prevents creating merge commits in your feature branch that later end up littering the `master` history.
