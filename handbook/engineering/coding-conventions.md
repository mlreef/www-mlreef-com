MLReef Coding Conventions
===================
The audience for our code is not computers, but rather our fellow developers.
This makes it our responsibility to write code which is easily understood by humans.



Kotlin
-------------------

We follow the official [Kotlin Coding Conventions](https://kotlinlang.org/docs/reference/coding-conventions.html)
with the following additions and exceptions …


### Continuation Indent Exception

We use `4` spaces instead of Continuation Indent instead of IntelliJ's default `8`.


### Kotlin's `!!` YOLO operator

Kotlins assertion operator (aka. YOLO operator) disables Kotlin's built in null-checking.
This means, if this assertion fails, it will throw an uncought NullPointerException with an empty message,
making it all but impossible to debug.

**Exception:** In test code the assertion operator is allowed, because the `!!` in itself is a type of assertion.


### File Naming
Kotlin files containing only a single non-private class, interface, or function
must have the same name as the class, interface, or function.

This naming is also the preferred naming for files containing only one main class
with helper classes which should be private but cannot be made private for framework reasons (e.g. REST DTOs).


`MyFooClass.kt`
```Kotlin
class MyFooClass {…}

priate fun supportFunction() {…}

private class HelperClass()
```

Kotlin files containing multiple public classes, interfaces, or functions shall
have a descriptive name. Using _Util_ is discurraged for reasons laid out below.

Kotlin files containing multiple declarations are to be named in `lower-kebab-case`.


`file-containing-multiple-declarations.kt`
```Kotlin
class MyFooClass {…}

priate fun support() {…}

private class HelperClass()
```


--------------------

Testing
--------------------
Testing of APIs (gitlab, redis, etc.) has to be performed against our own infrastructure or a dedicated test system. **NOT** against the production services (e.g. mlreef.com)


Are Util & Helpers evil?
--------------------

Yes! and No!
“Util” as a name does not contain any information. It is semantically the same as “random", “stuff", or at best "helpers".
It does neither help your fellow developer nor yourself to find a specific functionality.
Thus naming a class or a file just “Utils” is the same as not giving it any name at all - just a file containing random stuff.
Thing about "ConfigUtils" versus "ConfigAdapter" or "ConfigParser" - which one describes the functionality better?

But "util" and "helper" are even problematic on another level. They also does discourage the next contributor from good organisation.
Because there is already the messy util-file which already contains all kinds of functionality, another person is tempted to add their code in the same way - and so making the problem worse.

Because we want and need clear, expressive and self-documenting code, “util” as a name is highly discouraged and should only be used if it is the only useful naming option. 

In almost 100% of the cases, you can be more specific.

