Engineering
====================

[[_TOC_]]

Engineering Workflow
-------------------


Engineering standard operating procedures
-------------------
* [Introduce Early Access User](introduce-earlyaccess-user.md)
* [Merge Request](sops/merge-request-sop.md)
* [Weekly Developer Standup](sops/weekly-standup-sop.md)
* [Release to prodcution](sops/release-to-production-sop.md)


Product Roadmap
--------------------

MLReef's core features are:
create, develop and share Machine Learning projects.

Status of a feature
* mininmal 🔨
* viable 👍
* lovable ❤️


### Roadmap for MLReef's Alpha release

| Manage                     | Create                          | Build                  | Share                     | Release          |
| ----                       | ----                            | ----                     | ----                      | ----             |
| [User Management 🔨][384]  | [Minimal functionality 🔨][468] | [Experiments 🔨][463]    | [Code Repos 🔨][386]      |                  |
| [Group Management 🔨][464] | [Data Versioning 🔨][466]       | [Data Pipelines 🔨][465] | [Code Publishing 🔨][447] |                  |
|                            | [Data Sets 🔨][374]             |                          | [Marketplace 🔨][401]     |                  |
|                            | [Visualizations 🔨][467]        |                          |                           |                  |


[374]: https://gitlab.com/mlreef/frontend/-/issues/374
[384]: https://gitlab.com/mlreef/frontend/-/issues/384
[386]: https://gitlab.com/mlreef/frontend/-/issues/386
[401]: https://gitlab.com/mlreef/frontend/-/issues/401
[447]: https://gitlab.com/mlreef/frontend/-/issues/447
[463]: https://gitlab.com/mlreef/frontend/-/issues/463
[464]: https://gitlab.com/mlreef/frontend/-/issues/464
[465]: https://gitlab.com/mlreef/frontend/-/issues/465
[466]: https://gitlab.com/mlreef/frontend/-/issues/466
[467]: https://gitlab.com/mlreef/frontend/-/issues/467
[468]: https://gitlab.com/mlreef/frontend/-/issues/468



Engineering Managment
--------------------
At MLReef we are following an agile Scrum process with a two-week sprint cadence. Before each Sprint, we do a Sprint planning meeting in which stories are scoped for the next Sprint by the Frontend team and the DevOps team separately.

A story can be scoped if it is deemed as a priority by the Product Owner, the requirements are clear enough so that there are no more "missing_x" labels, and a work estimation was done by the respective team.

Sprints are modelled as milestones in Gitlab.

Scrum roles:
* Product Owner: @cpmlreef
* Scrum Master:  @rainerkern ???
* SPOC Frontend: @andres46
* SPOC Backend:  @flohin
* SPOC EPF:      @erika.torres
* SPOC Infra:    @rainerkern

### Requirements Engineering
The goal of requirements engineering is to define user stories "at arm's length" in an agile manner increasing the stories' detail-grade gradually as they move closer to the top of the Backlog.

We are applying a distributed process in which we hand over the process-ownership of separate phases between the stakeholding departments via their SPOCs (Single Point of Contact) thereby increasing quality criteria and meeting quality criteria.

#### Overview

Requirements engineering at MLReef can be summarized be the following processes:

| Defining                      | Coordinating                               | Specifying                                          | Doing                              |
| ----                          | ----                                       | ----                                                | ----                               |
| 1. PO defines user story      | 1. SPOC reviews EPICS + Sprints            | 1. Assignee sets `missing_xxx_requirement`          | 1. Assignee sets `workflow::doing` |
| 2. PO defines priority        | 2. SPOC sets lables `missing_xxx_planning` | 2. Assignee creates specs with other deps           |                                    |
| 3. PO assinges SPOC (FE/EPF)  | 3. Addressed SPOCs create missing tickets  | 3. Assignee sets `workflow::ready_4_implementation` |                                    |
| 4. PO sets ticket in backlog  |                                            | 4. Weights are set by the department                |                                    |
|                               |                                            | 5. PO sets in sprint                                |                                    |

**Need for meetings**

* **SPOC** needs to organize regular meetings (min every 2 weeks) to:
  * Review EPICS + Sprints
  * Address `missing_xxx_planning` -> review all close future tickets with this lable and create tickets

* **Assingee** needs to coordinate meeting with other departments (individual meetings, department meetings - e.g. FE/DevOps meeting) to define requirements.

* **ScrumMaster** organizes sprint and EPIC meetings with PO and at least department SPOCs (not necessarily all at once) -> Review forward planning.

#### Detailed definition of Requirements Engineering
[Workflow Board]: https://gitlab.com/mlreef/mlreef/-/boards/1801621
[Scheduling Board]: https://gitlab.com/mlreef/mlreef/-/boards/1776963

* **Defining:** Product Owner defines a new feature or a user story or as an _Epic_ in Gitlab.
  They write the description in the form of:  _Problem to be solved_ or _As a User, I want to XYZ_.
  They also create the first set of implementation tickets in collaboration with the department SPOCs.
  * Outcome 1: Ticket or Epic is created and recevies a _maturiy_, _priority_, and _roadmap_ label
  * Outcome 2: For Epics at least one implementation ticket is created, assigned to the ticket.
  * Outcome 3: The ticket(s) are labeled with a _priority_ (priority::low, priority::high, etc...)
  * Outcome 4: The ticket(s) are roughly scheduled in the [Scheduling Board]:
    Backlog, Priority Backlog, etc.
  * Outcime 5: If the ticket is too far in the future it is labeled with _workflow::defining_
  * Outcome 6: As soon as the Product Owner decides that a ticket is high enough on the Backlog, they add the label _workflow::coordinating_ on the [Workflow Board]

* **Coordinating:** Department SPOC reviews the upcoming stories and epics from the top of the Backlog (maximum ~4 to 6 Weeks)
  and organises periodic coordination for review and planning.
  In this phase the ticket should stay assigned to the SPOC.
  This way we reduce the tendency to write descriptions for a specific person building on their knowledge and maybe meetings they attended.
  We want the ticket to be described well enough so that every teammember could start the implementation
  * Outcome 1: If necessary, _missing_xxx_planning_ labels are added. This shows the other SPOCs and Product Owners that more work needs to be done.
  * Outcome 2: As soon as a corresponsing ticket was created the _missing_xxx_planning_ is removed and the ticket is added as _blocking_.
  * Outcome 3: As soon as possible, weight is estimated and set collaboratively by the team.
  * Outcome 4: As soon as all _missing_xxx_planning_ labels are removed the ticket is moved to _workflow::in_specification_ on the [Workflow Board]

* **Specifying:** The ticket's Assignee collaboratively creates the technical specification as well as one or more plans for the implementation,
  taking into account the required depth, technical risk, and uncertainties. Higher uncertainty implementation plans shall come with an alternative plan.
  The Assignee has ownership of coordinating specification meetings with team members or other department's SPOCs.
  * Outcome: As soons as the specification mature enough the ticket is moved to _workflow::ready_4_implementation_ on the [Workflow Board]

* **ready_4_implementation:** This story is ready to be added to be implemented during a sprint.

* **Doing:** During a sprint, as soon as a Developer starts actual development on the ticket, they apply the label _workflow::doing_ to the label. Either on the Development Board or [Workflow Board]



### User Story Workflow  
1. Requested: Ticket Status _Open_; owner: Product Owner
2. Specified: no implicit or explicit mapping
3. Estimated: Implicitly by having a weight defined; owner: Scrum Master
4. Planned: Implicitly by inclusion in either current or next sprint; owner: Scrum Master
5. Checked Out: Ticket Status _Doing_; owner: Ticket Assignee
6. Implemented: Merge Request created (not in WIP); owner Ticket & Merge Request Assignees
7. Deployed in Dev: implicitly by Merge Request _merged_
8. Deployed in Prod: no implicit or explicit mapping


Every Item on the Roadmap is represented by a corresponding Epic on the [Roadmap Board](https://gitlab.com/mlreef/frontend/-/boards/1526594?&label_name[]=Epic)

To facilitate estimations sometimes an [online planning poker tool](https://komplexitaeter.de/poker/?t=mlreef) is used.


**Planning Poker**

The backend reference story worth a **weight of 5 Points**:

```
In order for users to be able to manage their various Foos the MLReef backend has to provide a CRUD API.

Foos have the following properties: id, bar, baz, and ban
Foos have no relations to other entities.

### Implementation
* Database Migration
* Model Classes
* Spring Data Repo
* Service and Rest Controller
* Unit Tests
* Restdoc
```

The frontend reference story worth a **weight of 13 Points**:

```
As a User, I want to be able to manage my Foos in the MLReef frontend. I want to be able to:
* view all my different Foos in the Foo-overview (see mockup), 
* create new Foos
* update an existing Foo in the Foo-detail-view, and 
* delete a single Foo in the Foo-detail-view.

Foos have the following properties: id, bar, baz, and ban
Foos have no relations to other entities.
User input should be checked for (Numbers > 0, No empty strings, etc.)
```


Release Management
--------------------

### The Development Environment
Changes are automatically released to the _Develop_ environment as soon as a merge request is merged into the `develop` branch.
