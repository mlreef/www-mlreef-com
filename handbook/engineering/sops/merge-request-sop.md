[[_TOC_]]


Merge Requests
====================

Creating the Merge Request
--------------------
### Title
`#123 Add animated 404-error page to the MLReef website`

The title shall:
 * Include the ticket ID
 * Describe why this Merge Request exists
 * Finish the following sentence: If applied, this MR will ...
 * Short |<----  Using a Maximum Of 50 Characters  ---->|
 * Start with an uppercase letter
 * **NOT** describe what can be seen in the Git diff

Merge Requests at MLReef get squash-merged. When doing this, Gitlab uses the Merge Request Title as part of the commit message for the squashed commit;


### Description
If the title alone does not contain enough information, we use the description of the Merge Request to explain to the reviewers and other collaborateurs
what the Merge Request actually changes - in as many or few words as necessary.

This allows the reviewer to quickly grasp the Merge Request's context and provide higher quality reviews.

**Additionally:** The description shall contain references to _closed_ or _related_ tickets.

`closes: #17`

`relates to: #18, #19`

### Settings
 * **enable** `Delete source branch when merge request is accepted.`



Review & Approval
--------------------
In order to maintain high quality standards we are peer reviewing and testing each others code.
We take pride and ownership of this process.


### Review
In the **frontend** project the following default settings are configured:
 * Code Review by a second frontend developer
 * Frontend Testing by @cpmlreef (or @rainerkern as substitute)

> **Note:** Every developer can change the aproval settings for each for every merge request individually as needed.
This includes adding additional approvals as well as removing some or all approvals

After the Merge Request has been submitted it needs to be reviewed and approved.

As soon as the _Author_ wants his Merge Request to be reviewed, they assing the appropriate
person or persons to the Merge Request to give their approval. Usually a Merge Request will need multiple approvals,
thus it can be assigned to multiple people.

As a reviewer, if you are not satisfied with certain aspects, mention them in a comment.

If you have a question, assign the appropriate person.
This can mean to re-assign the original author to the Merge Request. 

Assigning one or more persons makes it explicit who has ownership of the Merge Request.

Assigning the author(s) to the Merge Request can be an indicator for them to do more work on the MR.

* Opening comment threads for source code lines effectively blocks an Merge Request from merging until the thread is resolved.
  This allows the Reviewer to approve the Merge Request, trusting in the author to take care of pending issues.

* As a reviewer, as soon as you are satisfied with the Merge Request, give your approval by clicking the _Approve_ button
in Gitlab's Merge Request overview page.

As long as there are other approvals needed **and** there are still other people assigned to the Merge Request,
after giving you approval, unassign (remove) yourself from the Merge Request.

```mermaid
graph TD
  START[/Start review\]
  R{Changes required?}
  COMMENT[Add comments]
  CANAPP{Approve?}
  UNASSIGN[Unassign myself]
  ASSAUTH[Assing Author or colleague]
  APPROVE[Approve]
  CANMRG{Can I merge?}
  MERGE[Merge]
  END[\Done for today/]

  START --> R
  R -->|yes| COMMENT
  COMMENT --> CANAPP
  R --> |no| APPROVE 
  CANAPP --> |yes| APPROVE
  CANAPP --> |no| UNASSIGN
  APPROVE --> CANMRG
  CANMRG --> |yes| MERGE --> END
  CANMRG --> |no| UNASSIGN
  UNASSIGN --> ASSAUTH --> END
```

The **last assignee** of the Merge Request carries the responsibility of the Merge Request beeing merged or reassigned.


Merging
----------------------
Unfortunately, Gitlab (inconsistently) somtimes takes the last commit message in a Merge Request and makes it the Squash-Message. This means, the person who merges a Merge Request is responsible for putting the Merge Request Title as the _Squash Commit Message_ into the Merge Request.

![image](merge-request-sop-message.png)

From gitlab docs:
```
The squashed commit's commit message will be either:

Taken from the first multi-line commit message in the merge.
The merge request's title if no multi-line commit message is found.

It can be customized before merging a merge request.
```
