[[_TOC_]]

Relase to Production standard operating procedure
====================

Merge Develop into Master
--------------------

Use your local Git client to merge the `develop` branch into the `master` branch.
Then push the master branch to the repository.

**Note:** This can only be done by project maintainers

```shell
git checkout master
git pull
git merge develop
git push
```


Cleanup the AWS runners
--------------------
At the moment still some manual intervention is necessary. There exists a [ticket](https://gitlab.com/mlreef/mlreef/-/issues/758) to better automate this part of the deployment.

After deployment to production was successfull the old runner ant its AWS machines have to be removed manually.
   
Log in to Gitlab -> http://PROD_IP.eu-central-1.compute.amazonaws.com:10080/admin/runners and find the runner which is no longer available.
You can easily identify the correct runner by the increasing `Last contact` time.
Make note of the `Runner token`

Now login to the AWS dashboard and terminate all machines which have the old Runner's `Runner Token` in their name.

> Hint: you can copy the Runner Token from gtilab into the AWS search field to quickly find all machines. Beware of spaces when copying!


Wait for the Pipeline
--------------------
After pushing the `master` branch Gitlab will run the CI/CD pipeline and will - given everything goes well - 
automatically deploy to the *Staging* environment on [staging.mlreef.com](https://staging.mlreef.com)


Do some manual smoke tests
--------------------
The standard procedure of manual testing is: 

> As a very general rule: Review the latest MRs included in the staging environment and focus on testing these.

Follow the standart testing by: 
1. Creating a new user
2. Creating a new ML project
3. Upload new data using the data uploader or git
4. Create a new dataops pipeline that generates a new dataset with corresponding data operations (based on the data type you uploaded)
5. Review the pipeline success (status, time, any relevant log messages)
6. Create a new Merge Request out of the newly created dataset
7. Create a new data visualization (if possible on the data type selected) and review the outcome
8. Create a new experiment and execute it (based on the data type you uploaded)
9. Review the experiment status, the output, download the artifacts, etc.
10. Misc. testing: review settings, permissions, access levels to other projects, etc...



Deploy to Production
--------------------
Use the "Deploy to Prodcution" Job in the [Infrastructure's CI/CD pipeline](https://gitlab.com/mlreef/infrastructure/-/pipelines) to deploy the current *Staging* version to *Production*
