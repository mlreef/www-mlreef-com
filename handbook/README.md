This is MLReef team handbook is the central repository for how we run the company.
**EVERYONE** shall contribute to this handbook by documenting current practises
but more importantly suggest improvments.

As part of our value of being transparent. The handbook is open to the world, 
and we welcome feedback  Please make a merge request to suggest improvements or 
add clarifications. More information about how we are using the handbook can be found [here](about.md).

Definitions for commonly used terms can be found in [the glossary](glossary.md)


Company
====================
* [Core Values](#core-values)
* [Out High Standards](#our-high-standards)


Engineering
====================
* [Core Values](#core-values)
* [Our High Standards](#our-high-standards)


People Operations
====================
* [Code of conduct](code-of-conduct.md)
* Roles at MLReef
  * CEO
  * [CTO](people-operations/chief-technology-officer.md)
  * [Backend Developer](people-operations/backend-developer.md)
  * [Frontend Developer](people-operations/frontend-developer.md)
  * [Machine Learning Engineer](people-operations/machine-learning-engineer.md)


------

------

------


Core Values
--------------------
We are MLReef, we give people the power to change the world. Our mission is to nurture the world's ambition to create artificial intelligence.

MLReef is a high tech company for the people. We equalise the playing field and give everyone a chance to participate in the world of machine learning on their own merits.

We are unique; unique in our thinking; unique in our way of working; and unique in our way of organising ourselves.

The company MLReef is based on the following three core values. These values inform everything that we do. 


Our high standards
--------------------
Working at MLReef is always governed by our **core values**. We hold ourselves to high standards in the way we conduct ouselves and also in the way we are performing our work. You can read a more detailed description on this [handbook page](working-model.md)


### Trust
We give everyone who joins us or supports us a lot of trust and responsibility. We act under the assumption that everyone will do the right thing to bring us closer to our goal.

We expect everyone to follow the rules and procedures in this handook.

We expect and encourage everyone to take on responsibility. If you think of a better way to achieve a goal. Change the rules, change the procedures following our [handbook first approach](working-model.md). You are obligated to do that until the right thing happens.


### Communication
Anyone can and should talk, phone, email, message to anyone else. We value clear and concise, low-context, asynchronous, non-intrusive, and frequent communication. Our communication is done in English, so it is always possible to share conversations with other team members.

see our [communication culture](#communication-culture)


### Job Duties
It is your respnsiblity to understand what is expected of you. Your manager and you should define and update your responsibilities regularly. If you are unclear, ask. "Id did not know, no one told me" is not an excuse that is axcepatable at MLReef.

You should have a [bias for action](working-model.md), especially if you are the [directly responsible individual](working-model.md)


### Attendance
Be the kind of person that your team can rely on. Be there when you are needed, punctual and prepared; We can not get things done when your are not here. 

Do not be late. If you are late, let your colleagues know as soon as possible. They are reasonable and respectful if you are.


### Progress is better than perfection
Aka _minimal viable merge request_.


### Outside employment
You may work on other projects while you are a member of MLReef as long as you perform your job here well and are not compromising confidentiality or trade secrets. Your will be judged by the same standards as everyone else.


### Fun
Make sure you are having fun while working. Meet new friends, be happy, love what you do.




Engineering
--------------------
This section details the development processes of MLReef.

### Communication Culture
Engineering values clear and concise, low-context, asynchronous, non-intrusive, and frequent communication. Our communication
is done in English, so it is always possible to share conversations with other team members.  

What does that mean?

* **Clear and concise:** We aim to be clear in our communication. Each communication artifact (eg. an Email, comment, ticket, etc.)
  should require as little context as possible, already including descriptions, clear references like ticket numbers and links,
  and, if not completely obvious, some context to the regards of the message.
  
  This also means actively keeping topics on one channel, as far as possible and **NOT** scattering the communication
  between Gitlab comments, slack messages and emails - which would make it impossible to follow the flow of the conversation
  or share it with other team members.  
  
  This also means to keep conversations on-topic, to better facilitate positive and productive discussions.
 

* **Low-context:** There exists a great [Wikipedia article](https://en.wikipedia.org/wiki/High-context_and_low-context_cultures#Examples_of_higher_and_lower_context_cultures)
  about the difference of high context and low context cultures, which contains concepts which are also applicable here.
  > Typically [in] a low-context culture […] individuals communicating will have fewer relational cues when interpreting messages.
  > Therefore, it is necessary for more explicit information to be included in the message […].
  > Not all individuals in a culture can be defined by cultural stereotypes […].
  > However, understanding the broad tendencies [… can help …] to better facilitate communication between individuals …
  
  **Typical Higher-context cultures:** African, Arabic, Persian, most Asian, most southern European, Slavic, Russian, Indians, Latin Americans
    
  **Lower-context cultures:** Almost all Anglo-Germanic, Israelis, Scandinavian

* **Asynchronous:** Especially engineers, need a lot of uninterrupted time to work. Realtime channels like Slack, Calls and Meetings,
interrupt this flow (see also the [makers schedule](working-model.md)). By actively choosing asynchronous, non-realtime
channels like email and Gitlab Comments, we allow the recipients to answer on their own time.

* **Non-intrusive:** Notifications are distracting to the receiver. Every time you send an avoidable notification to someone
you are wasting their time by disrupting their workflow and increase their anxiety and stress levels.
On the other hand, of course, interrupting is sometimes necessary, for urgent matters. This is why we aim to
minimize notifications to help our teammates. This has the added benefit of making the remaining notifications count more.

At MLReef we have a plethora of communication channels at our disposal. Email, Slack, Gitlab Comments and Mentions,
Google Meet, Skype, Whats App, Telephone, and more. Each of those channels brings their own communication culture with it.
Additionally, each culture, age group and even individual uses those channels differently.
Thus `number_of_channels x individuals x number_of_cultures = total_amount_of_different_expectations`

In this section we are trying to define a framework of communication behaviours which are globally accepted within MLReef.

#### MLReef communication framework
* Conversations regarding Tickets and Merge Requests are held within the ticket comments.
* Threads are used to separate different topics within Tickets, Merge Requests and Slack Channels
* The Slack channel "MLReef-Community" serves for convesation and support to and between our community. Please see this [guide](mlreef-community).

#### Response Times
* Slack: between minutes and hours
* Email: between Hours maximum two days
* Gitlab: between half days till the end of the week 



