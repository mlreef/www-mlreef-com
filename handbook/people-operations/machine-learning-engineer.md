Machine Learning Engineer Roles at MLReef
====================

ML engineers at MLReef work directly with our product. This includes both the community edition as well as the enterprise edtition. They work together with the frontend developers, the machine learning developers to solve common goals.

Machine Learning Engineers roles at MLReef share the following requirements and responsibilities.


Requirements
-------------------
* Demonstrated knowledge to design, organize and create state-of-the-art Machine Learning projects
* Ability to understand key value added tasks and processes during the development of Machine Learning use cases
* Deep expertise in Python and its data science libraries such as scikit-learn, pandas, NumPy, TensorFlow
* Ideally demonstrated real-world use cases, either with clients or on own projects scoping ideally different ML fields (e.g. NLP, time series, image classification, etc).
* Demonstrated capacity to clearly and concisely communicate about complex technical, architectural, and/or organizational problems and propose thorough iterative solutions
* Comfort working in a highly agile, intensely iterative software development process
* Demonstrated ability to onboard and integrate with an organization long-term positive and solution-oriented mindset
* Effective communication skills: Regularly achieve consensus with peers, and clear status updates
* An inclination towards communication, inclusion, and visibility
* Self-motivated and self-managing, with strong organizational skills
* Demonstrated ability to work closely with other parts of the organization
* Share our values, and work in accordance with those values
* Ability to thrive in a fully remote organization
* Ability to use GitLab and MLReef

### Nice to have:
* Experience in the field of Machine Learning
* Working knowledge of ReactJS, or Ruby on Rails
* Experience in a peak performance organization, preferably a tech startup
* Experience with the MLReef product as a user or contributor
* Product company experience
* Experience working with a remote team
* Enterprise software company experience
* Developer platform/tool industry experience
* Experience working with a global or otherwise multicultural team
* Experience contributing to open source software
* Domain knowledge relevant to the product stage in which you are looking to join


Responsibilities
-------------------
* Testing the limits of MLReef by creating / porting state-of-the-art models into MLReef
* Designing and creating MLReef compatible use cases with exemplary data sets, data operations & visualizations and experiments (models) for specific, real-world use (e.g. predictive maintenance, churn prediction, object detection, etc).
* Designing light-house use cases (e.g. working with ESA to create a model to classify reforestation efforts in the high Atlas region; measuring ice-cap movement and predicting paths with ML) for marketing purpose
* Defining core functions of MLReef together with the product development department
* Providing impulses for the product development department on new features and functions related to own and general market activities
* Supporting the marketing department to communicate features and functions
* In some cases helping key partners perform their ML projects on MLReef


Trials / Test Project
--------------------
The goal for us is to get a holistic view on your work and communication skills. We think the best approach is to let you choose a use case (project) of your choice and submit the following:
 
1. A data set or a sufficient sizeable subset - ideally open source; goal is to understand the use case and the fitting data for that use case
2. A set of preprocessing operations needed to create a trainable data instance – no more than 3 as it will be taken just to understand the problem solving capabilities for a given data set / structure
3. A model to be trained, including a description on the parameters choice – ideally open source, can be a pre-trained model; We want to get an idea why you chose this model + params
4. A readme ideally written in markdown with a brief but concise description about the choices and steps taken
Submit via GitHub, Gitlab, Docker, ZIP or any other means
 
Please, before starting, let us know about your salary wishes. The test will be remunerated.
