Backend Developer Roles at MLReef
====================

Backend developers at MLReef work directly on our product. This includes both the community edition as well as the enterprise edtition. They work together with the frontend developers, the machine learning developers to solve common goals.

Backend Developer roles at MLReef share the following requirements and responsibilities.


Requirements
-------------------
* Professional experience with Kotlin, Spring, JPA and/or other modern Java and Kotlin frameworks
* Writing code follwing [SOLID princiles](https://en.wikipedia.org/wiki/SOLID) and [TDD principles](https://www.youtube.com/watch?v=qkblc5WRn-U)
* A solid understanding of IOC - Dependency Injection, Software Design, and Architecture patterns
* A solid understanding in core web and database concepts (eg. how requests are authenticated)
* Proficiency in the English language, both written and verbal, sufficient for success in a remote and largely asynchronous work environment
* Demonstrated capacity to clearly and concisely communicate about complex technical, architectural, and/or organizational problems and propose thorough iterative solutions
* Experience with performance and optimization problems and a demonstrated ability to both diagnose and prevent these problems
* Comfort working in a highly agile, intensely iterative software development process
* Demonstrated ability to onboard and integrate with an organization long-term positive and solution-oriented mindset
* Effective communication skills: Regularly achieve consensus with peers, and clear status updates
* An inclination towards communication, inclusion, and visibility
* Self-motivated and self-managing, with strong organizational skills
* Demonstrated ability to work closely with other parts of the organization
* Share our values, and work in accordance with those values
* Ability to thrive in a fully remote organization
* Ability to use GitLab and MLReef
* Working knowledge of Python, or at least the willingness to learn 

### Nice to have:
* Experience in the field of Machine Learning
* Working knowledge of ReactJS, or Ruby on Rails
* Experience in a peak performance organization, preferably a tech startup
* Experience with the MLReef product as a user or contributor
* Product company experience
* Experience working with a remote team
* Enterprise software company experience
* Developer platform/tool industry experience
* Experience working with a global or otherwise multicultural team
* Experience contributing to open source software
* Domain knowledge relevant to the product stage in which you are looking to join


Responsibilities
-------------------
* Solve technical and architectural challenges of high complexity on a global, MLReef-wide scope
* Drive innovation on the team with a willingness to experiment and to tackle problems of high complexity and scope
* Exert influence on the overall objectives and long-range goals of your team and the company
* Collaborate with Product Management and other stakeholders within Engineering (Frontend, UX, etc.) to maintain high quality in a fast-paced, iterative environment
* Confidently ship moderate size features and improvements in a secure, well-tested, and performant way, with minimal guidance and support from other team members. Collaborate with the team on larger projects
* Actively seek and advocate for improvements to product quality, security, performance, and reduction of technical debt
* Help to define and improve our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review
* Provide mentorship for other Engineers to help them grow in their technical responsibilities and remove increase their autonomy
* Craft code that meets our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code reviews and other measures
