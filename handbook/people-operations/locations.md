MLReef's unofficial office locations
====================

Goldegg "Office"
--------------------
MLReefs official headquarters are located in a tiny little town called Neidling in Camillo's home.

![Goldegg Satelite view](goldegg-map.png)


### Team members at this location
* [Camillo Pachmann](mailto:cp@mlreef.com)


### Visiting Goldegg
Camillo and his wife  are great hosts and especially during summertime we try to work from Goldegg at least once a week.
The office during those months is usually the so called "Drinnen-Draussen" terrace located behind the house. 

Internet coverage is kind of sketchy but this is compensated by unlimited coffee at the neighbours kitchen Mrs. Judie and Mr. Andreas Auersperg.

Usually, when going Goldegg we are having a joint potluck breakfast and eat lunch together at 13:00. Lunch is often
together with other guests or neighbours. 

There are a number of farm animals living freely on the property as well as at least two dogs Jacko and Tonx.
All the animals are super nice an playful.



Vienna Office
--------------------
The Vienna "office" of MLReef is located at [Stockwerk Coworking Space](https://www.stockwerk.co.at/) in Vienna's "Rudolfsheim" district.

![Stockwerk Map](stockwerk-map.png)

Located at the 3rd floor of the building. We are sharing this office with another Vienna based
SAAS Company represented by their CTO Christoph.

![Photo 1](vienna-office-1.jpg)

![Photo 2](vienna-office-2.jpg)


### Teammembers at this location
* [Rainer Kern](mailto:rainer.kern@mlreef.com)


### Getting there
The easiest way to get to Stockwerk via the subway. It is only a 5 to 7 minute walk from the "Westbahnhof" trains station.
There are usually also parking spots available, tough they are paid short term parking - as is most of Vienna. 


### Office Rules
The permanent "residents" have unlimited access to the coffee machine on the first floor. Visitors will have to pay 1€ per cup'o joe.

There is a coffee machine in the office, which is paid for jointly and privately by the people sharing the office.  

Since we are sharing the office, having prolonged calls or meetings is discouraged inside the office.


### Lunch
Stockwerk organises a group lunches almost every day:
* Monday: Freshly cooked menu by an external cook - usually ~9€
* Tuesday: Pasta - usually 2,50€ 
* Wednesday: Freshly cooked menu by an external cook - usually ~9€
* Thursday: joint delivery order
* Friday: joint pizza delivery - usually 8€
