Qualification Process
====================

Welcoming new persons into our community is something we take very seriously at MLReef.
This page lays out how to ensure that candidates are a good fit for the MLReef crew,
not only meeting our technical skill requirements but also being a good cultural fit.


The qualification process consits of the following stages:
* **Application Review:** After an application has been received it will be reviewed either by Camillo and/or Rainer (depending on the position).
* **First interview:** The first interview is to get to know the candidate and have a first interview.
  * Interviews with @rainerkern can be booked on [calendly](https://calendly.com/rainer-kern-mlreef/meeting)
  * Interviews with @cpmlreef can be booked via email `cp@`
* **Short paid trial:** After having made sure that the technical requirements have been met, we invite the candidate to solve a small ticket for us. This should be concluded with one working week - including some time allocation for getting to know the project setup
* **Welcoming to the team:** As a last step, officially welcome the new team member following the [welcoming sop](welcoming-sop.md)


### Questions to be answered during the first interview
* **Working Remotely:** Will you be fine working remotely (home office)?
* **Availability:** From when on could you start?
* **Handover current project:** Will you need a handover time with your current team? (e.g. one day per week for four weeks)
* **Business Registration:** Do you already have a business / freelancer registration which allows you to issue invoices in your country?
* **Payment:** Can you reveive bank transfers (IBAN) or transfers via Paypal
* **Training:** What was the last book or course that you have done
* **Artifacts:** What are the main artifics you produce
* **Company Values:** Can the candidate provide a story for living out out company values
* **Why do you want to work for us:** What are you expecting / what is the thing that is missing from your current workplace or project.


### Other Questinos
Getting to know a new candidate - for us - is more than just ticking off checkmarks on a list.
It also entails having a conversation in order to see if the new candidate is also a fit on a personal level.
Often, during an interview this happens somewhat naturally.
Additionally, here we have some questions which help us in this process.

* What was the coolest part of your last job - not necessarily only work related
* How did you relate to your colleagues
* What would be a personal goal you want to achieve in the next year?

