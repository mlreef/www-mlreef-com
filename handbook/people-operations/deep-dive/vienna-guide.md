Travel Guide to Vienna
====================
Vienna - a romantically imperial city - is a dream city for anyone with a romantic streak or an interest in history, music, and culture. You can find an abundance of sightseeing opportunities while wondering along medival alleys or across imperial squares and streets. Visiting Schönbrunn Palace with its parks and statues, and the Imperial Palace (Hofburg) while waling in the footsteps of Sissi and Emperor Franz Josef., and marvel at the majestic architecture along the Ring boulevard.
Vienna also boasts the comforts and infrastructure of a modern city!


Culture
--------------------
[Walking tour of the city center (free/donation)](https://www.welcometourvienna.at/)


Cuisine
--------------------
Lots of our typical foods are shared, influenced, imported from former provinces of the Austro-Hungarian empire
* [Wiener Schnitzel (Viennese escalope)](https://en.wikipedia.org/wiki/Wiener_schnitzel)
* [Schweinsbraten (pork roast)](https://duckduckgo.com/?q=wiki+schweinsbraten&ia=web)
* [Wiener Saft Goulash](https://www.wien.info/en/shopping-wining-dining/viennese-cuisine/recipes/fiaker-goulash)
* [Käsekrainer (carnolian sausage with cheese inside)](https://en.wikipedia.org/wiki/Carniolan_sausage)
* Heurigen (Vine Tavern) Food in general
* [Apfelstrudel (Viennese Apple Pie)](https://en.wikipedia.org/wiki/Apple_strudel)
* [Sachertorte (Sacher Cake)](https://www.sacher.com/de/original-sacher-torte/)
* [Aida's Mozarttorte (Mozart Cake)](https://www.tripadvisor.com/LocationPhotoDirectLink-g190454-d1959272-i55039475-Cafe_Konditorei_Aida-Vienna.html)


**Sources**

* [wien.info](https://www.wien.info/en/shopping-wining-dining/viennese-cuisine)


Costs of living
--------------------

| Item                                   | Price per person per day |
| ---                                    | ----------- |
| Hotel double room                      | from ~50€   |
| Groceries for three meals              | 10€ - 15€   |
| Good bottle of wine (supermarket)      | > 10€       |
| Restaurant lunch (simple meal + drink) | 15€ - 20€   |
| Restaurant dinner (medium + drinks)    | 25€ - 35€   |
| Café Espresso                          | ~2,9€       |
| Café Cappuccino                        | ~3,5€       |
| Softdrink in a bar 0,25 - 0,3L         | ~2,5€ - 4€  |
| Beer in a bar 0,5L                     | ~3,5€ - 5€  |



**Sources**
* [Booking.com Hotel page](https://www.booking.com/searchresults.en-gb.html?label=gen173nr-1FCAEoggI46AdIM1gEaA6IAQGYAQm4AQfIAQzYAQHoAQH4AQuIAgGoAgO4ArK78_AFwAIB&sid=a916172ca12564d5e80188db08b63584&tmpl=searchresults&checkin_month=3&checkin_monthday=5&checkin_year=2020&checkout_month=3&checkout_monthday=16&checkout_year=2020&city=-1995499&class_interval=1&dest_id=-1995499&dest_type=city&dtdisc=0&from_sf=1&group_adults=2&group_children=0&inac=0&index_postcard=0&label_click=undef&no_rooms=1&postcard=0&room1=A%2CA&sb_price_type=total&shw_aparth=1&slp_r_match=0&src=searchresults&srpvid=bd65958d2bad013a&ss=Vienna&ss_all=0&ssb=empty&sshis=0&ssne=Vienna&ssne_untouched=Vienna&top_ufis=1&nflt=pri%3D1%3B&rsf=)
* [Billa grocery store online shop](https://www.billa.at/warengruppe/obst-und-gemuese/obst/B2-11)
