# MLReef Values

MLReef´s core values are collaboration, results, efficiency, transparency, diversity and sustainability. 


## About our values

Our values are defined by the people who make MLReef - the community of users and the talents working day to day on our company. These values are a living part of MLReef and
are due to gain or lose importance. But they are meant a guideline for all the people around MLReef to work and thrive together. As such, everyone is welcome to suggest
improvements.


## Value elements

We defined elements in values to substatieate better our values, to make them touchable and livable. They remove ambiguity and create context. 
Value elements clarify what a given value means and looks like at MLReef. 

Understanding the distinction is critical to thriving in MLReef, especially for newer team members to integrate better to our way of work.


# Collaboration

Helping others is a priority, even when it is not immediately related to the goals that you are trying to achieve. Similarly, you can rely on others for help and advice — in fact,
you're expected to do so. Anyone can chime in on any subject, including people who don't work at MLReef. The person who's responsible for the work decides how to do it, but 
they should always take each suggestion seriously and try to respond and explain why it may or may not have been implemented.

### Caring

We value caring for others. Demonstrating we care for people provides an effective framework for challenging directly and delivering feedback. Feedback most often need both
sides to work properly - the giver and the receiver. Kindness and caring is the key for accepting what the other has to say. 

### Negative feedback

Give negative feedback in the smallest setting possible. One-on-one video calls are preferred. If you are unhappy with anything (your duties, your colleague, your boss, your salary, your location, your computer), please let your boss, 
or the CEO, know as soon as you realize it. We want to solve problems while they are **small**.

Negative feedback is different from negativity or disagreement.

### Be on time

Collaboration works best when feedback or the response to feedback, help or any other form of interaction is on time. This will help to solve problems quickly, while they are
still small, give room for reaction and help coordination and communication. For urgent matters, do not hesitate to use our main channel of internal communication. 

### Get to know each other

MLReef is all remote, which means that we use text communication most of the time but surely don´t see each other on many ocasions. Getting to know each other on a more personal
level helps to understand each other better through our company calls, virtual coffee breaks or our annual MLReef Deep Dive. 

### Don´t pull rank

If you have to remind someone of the position you have in the company, you're doing something wrong. People already know our decision-making process. Explain why you're
making the decision, and respect everyone irrespective of their function. This includes using the rank of another person - including the CEO - to sell an idea or decision.

### Say sorry 

If you made a mistake, apologize as soon as possible. Saying sorry is not a sign of weakness but one of strength. The people that do the most work will likely make the 
most mistakes. Additionally, when we share our mistakes and bring attention to them, others can learn from us, and the same mistake is less likely to be repeated by 
someone else. 

### Don´t let each other fail

Keep an eye out for others who may be struggling or stuck. If you see someone who needs help, reach out and assist, or connect them with someone else who can provide expertise 
or assistance. We succeed and shine together!


