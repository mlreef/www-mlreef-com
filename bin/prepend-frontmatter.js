const fs = require('fs');

const flat = items => items.reduce((acc, i) => Array.isArray(i) ? [...acc, ...i] : acc.concat(i), [])

const createFrontmatter = props => `---
${Object.entries(props).map(([key, val]) => `${key}: ${val}`).join('\n')}
---`

const getFilenamesFromDirTree = (path, rootDir = `${__dirname}/../`) => new Promise((resolve, reject) => {
    fs.readdir(rootDir + path, { withFileTypes: true }, async (err, filenames) => {
      if (err) throw err;

      const allFiles = await Promise.all(
        filenames.map(async dirent => !dirent.isDirectory()
          ? `${rootDir}${path}/${dirent.name}`
          : await getFilenamesFromDirTree(dirent.name, `${rootDir}${path}/`))
      );

      resolve( flat(allFiles) )
    });
});

const prependHeader = (file, header) => {
  const text = fs.readFileSync(file);
  const content = `${header}\n\n${text}`;

  return fs.writeFileSync(file, content)
}

const frontmatter = createFrontmatter({
  type: 'handbook'
});

getFilenamesFromDirTree('content/handbook')
  .then( filepaths => filepaths.filter(path => /\.md$/.test(path)) )
  .then( mdpaths => mdpaths.map(fp => prependHeader(fp, frontmatter)) );
