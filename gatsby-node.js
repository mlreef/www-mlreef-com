const path = require('path');
const { slugify } = require('./src/misc/dataParsers');

exports.createPages = async ({ actions, graphql, reporter }) => {
  const { createPage } = actions;
  const postTemplate = path.resolve('src/templates/post.jsx');
  const result = await graphql(`
    {
      allMarkdownRemark(
        sort: { order: DESC, fields: [frontmatter___date] }
        limit: 1000
      ) {
        edges {
          node {
            html
            excerpt
            frontmatter {
              title
              author
              date(formatString: "MMM DD, YYYY")
              image
            }
          }
        }
      }
    }
  `);

  // Handle errors
  if (result.errors) {
    reporter.panicOnBuild('Error while running GraphQL query.');
    return;
  }

  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: `blog/${slugify(node.frontmatter.title)}`,
      component: postTemplate,
      context: node,
    });
  });
};
