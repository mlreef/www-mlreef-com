# Development Guidelines
This is a web application based on [Gatsby Advanced Starter](https://github.com/Vagr9K/gatsby-advanced-starter) which has been through many modifications, but keeping the spirit of allowing non-web developers to easily add content written in *Markdown* and configure *json* files.

## Project Structure
+ `data/config.yaml` contains configuration for header, footer, and home page components.
+ `handbook/` contains the MLReef Handbook.
+ `static/` for all static files, robots.txt included.
+ `content/` contains blogposts, and other public files.
+ `src/common/` currently contains global styles.
+ `src/components/` For all components used throughout the application.
+ `src/data/` static files, mostly legal information.
+ `src/layout/` are layout design components, every page is wrapped by one.
+ `src/misc/` helpers functions.
+ `src/pages/` is where all static pages are located.
+ `src/templates/` for templates used for dynamic pages such as posts.

## Basic configuration
The `gatsby-config.js` contains the basic information about MLReef such as description, contact, keywords, etc. and also the gatsby plugins configured:

+ `gatsby-plugin-polyfill-io` for browser compatibility
+ `gatsby-plugin-sass`
+ `gatsby-transformer-remark` essential for *markdown* parsing
+ `gatsby-transformer-yaml` for parsing `data/config.yaml`
+ `gatsby-source-filesystem` allows directories in *GraphQL*
+ `gatsby-plugin-react-helmet` for SEO
+ `gatsby-plugin-manifest`
+ `gatsby-plugin-sitemap` create the sitemap during build stage
+ `gatsby-plugin-brotli` compress files for SEO performance


## Cookie compliance
MLReef uses [Cookiebot](https://www.cookiebot.com/) for *RGPD* and cookies management.

Two scripts cookiebot-consent-popup and cookiebot-declaration are included in `gatsby-ssr.js` to enable the *cookiebot*, notice that this configuration only works for production and in review environments might appears console errors.

Some style customization is located in [src/pages/index.scss#L136](src/pages/index.scss#L136) and it's recommended to move it to `src/common/styles.scss`.


## Blog
Post are located in `content/blog` each one in every file. Post are written in *markdown* and must include a *frontmatter* as heading. See post examples.

Post pages are created dynamically, see `gatsby-node.js`.

Other resources like images are located in `static/images/blog/`.


## Press resources
In the route `/press` users can find logos and other material related to **MLReef**. This files are located in `content/resources/` and they are dynamically rendered using *GraphQL*, hence it's only needed to put the resource in that directory. See [PressCard](src/components/PressCard) component for more information.

Every image will be shown as a downloadable item and files with same name (and different extension) will be grouped.

In addition it's possible to add a title, for this you need to look at `data/config.yaml` the field **press.titles** where **resource** means to the filename
and **text** the title for the image.

## Styles
The main **MLReef** [theme](externals/styles) is used, and all custom styles are written in *SCSS* following [MLReef style guidelines](https://gitlab.com/mlreef/mlreef/web/src/styles).

## Externals
Modules that are imported from other projects (mainly [MLReef](https://gitlab.com/mlreef/mlreef)) are located in this folder. Remember the version used might not match the original master version.


## Scripts available
+ `npm run develop` or `npm start` start the development server
+ `npm run clean` clean cached files, use it in case of errors
+ `npm run serve` start a static server for the `public/` directory
+ `npm run build` built the production in `public/`
+ `npm run build:serve` clean, build and start the static server
+ `npm run deploy` run a script that deploys in a hosting through ssh. [see](bin/utils)
+ `npm test` start test suit (no test yet)
+ `npm run format` execute prettier


## Performance
The recommended tool for performance metrics is [Lighthouse](https://developers.google.com/web/tools/lighthouse?hl=es). The last analysis scored:

![image](static/images/performance-2021-02-23.png)

Other useful tools:

+ [Google Console](https://search.google.com/)
+ [Structured Data Test](https://search.google.com/structured-data/testing-tool)
+ [Meta Tags](https://metatags.io/)
