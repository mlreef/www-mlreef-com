export const getBySearchableType = async (parametersArr = []) => {
  const searchUrl = new URL('https://mlreef.com/api/v1/explore/entries/search');

  parametersArr.forEach(({ name, value }) => {
    searchUrl.searchParams.append(name, value);
  });

  const data = await fetch(new Request(
    searchUrl.href,
    { method: 'POST' },
  )).then((res) => res.json());

  return data;
};
