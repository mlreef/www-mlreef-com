import { getBySearchableType } from '../apis/projectApis';

export async function processApiCall(arrayOfParameters = []) {
  const data = await getBySearchableType(arrayOfParameters);

  return data.content;
}
