import React, { useState, useEffect } from 'react';
import cx from 'classnames';
import './ChangingText.scss';

const ChangingText = props => {
  const {
    className,
    words,
    interval,
    uppercased,
    style,
  } = props;

  const [displayedList, setDisplayedList] = useState(words);

  useEffect(
    () => {
      const rotateList = () => {
        setDisplayedList(ws => [...ws.slice(1), ws[0]]);
      };

      const id = setTimeout(rotateList, interval);
      return () => clearTimeout(id);
    },
    [displayedList, setDisplayedList, interval],
  );

  return (
    <span className={cx('changing-text', className)} style={style}>
      {displayedList.map(word => (
        <span
          className="changing-text-word"
          key={`ct-${word.content}`}
          style={{ color: word.color }}
        >
          {uppercased ? word.content.toUpperCase() : word.content}
        </span>
      ))}
    </span>
  );
};

ChangingText.defaultProps = {
  className: '',
  interval: 2000,
  uppercased: false,
  style: {},
};

export default ChangingText;
