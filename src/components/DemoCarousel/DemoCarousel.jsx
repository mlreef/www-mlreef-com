import React from 'react';
import cx from 'classnames';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './DemoCarousel.scss';

const DemoCarousel = (props) => {
  const {
    className,
  } = props;

  return (
    <section className={cx('demo-carousel', className)}>
      <Slider
        dots
        infinite
        speed={500}
        slidesToShow={1}
        slidesToScroll={1}
      >
        <div>
          <div className="demo-carousel-container">
            <div className="demo-carousel-content featured-list">
              <div className="featured-list-content">
                <h3>
                  A modular approach to ML
                </h3>
                <p>
                  MLReef introduces modules into the Machine Learning life cycle.
                  A module is a fundamental part which is defined by its final purpose,
                  for example to create a data processing or train a model.
                </p>
                <ul className="featured-list-content-list">
                  <li className="">
                    Add modularity to your ML flow
                  </li>
                  <li className="">
                    Access team and community made modules
                  </li>
                  <li className="">
                    Construct your ML projects module by module
                  </li>
                </ul>
              </div>
            </div>
            <div className="demo-carousel-image border-rounded">
              <picture>
                <source srcSet="/gifs/mlreef_dashboard.webp" type="image/webp" />
                <img
                  src="/images/fallback/MLReef_collections.png"
                  alt="mlreef projects demo dashboard"
                />
              </picture>
            </div>
          </div>
        </div>
        <div>
          <div className="demo-carousel-container">
            <div className="featured-list demo-carousel-content">
              <div className="featured-list-content ">
                <h3>
                  Host and publish modules using CI/CD
                </h3>
                <p>
                  In MLReef you host your modules using git repositories. Remove the
                  barrier between DevOps and Data Scientists with our integrated CI/CD
                  functionality, that builds the required environment so that anyone in
                  the team can immediately run your modules.
                </p>
                <ul className="featured-list-content-list">
                  <li className="">
                    Automate your DevOps for reproducibility
                  </li>
                  <li className="">
                    Update your code modules continuously
                  </li>
                  <li className="">
                    Customize your environment for your requirements
                  </li>
                </ul>
              </div>
            </div>
            <div className="demo-carousel-image border-rounded">
              <picture>
                <source srcSet="/gifs/mlreef_experiments.webp" type="image/webp" />
                <img
                  src="/images/fallback/MLReef_experiments.png"
                  alt="mlreef projects demo experiments"
                />
              </picture>
            </div>
          </div>
        </div>
        <div>
          <div className="demo-carousel-container">
            <div className="featured-list demo-carousel-content">
              <div className="featured-list-content">
                <h3>
                  Running jobs is a simple low-code task
                </h3>
                <p>
                  MLReef makes it easy to adopt and (re)use code based modules for
                  running data processing, data visualization or model training.
                  This modularity enables orchestration between team members and
                  shareability with other users.
                </p>
                <ul className="featured-list-content-list">
                  <li className="">
                    ML is as easy as drag and drop
                  </li>
                  <li className="">
                    Save time with ready to use modules (no setup, no code)
                  </li>
                  <li className="">
                    High flexibility and ownership of the source code
                  </li>
                </ul>
              </div>
            </div>
            <div className="demo-carousel-image border-rounded">
              <picture>
                <source srcSet="/gifs/mlreef_pipeline.webp" type="image/webp" />
                <img
                  src="/images/fallback/MLReef_pipelines.png"
                  alt="mlreef projects demo experiments"
                />
              </picture>
            </div>
          </div>
        </div>
        <div>
          <div className="demo-carousel-container">
            <div className="featured-list demo-carousel-content">
              <div className="featured-list-content">
                <h3>
                  Run, reproduce and compare experiments
                </h3>
                <p>
                  By using the power of GIT you can retrieve the complete history of
                  your ML projects to optimize your efforts. Run multiple experiments
                  in parallel without worrying about the computational resources.
                </p>
                <ul className="featured-list-content-list">
                  <li className="">
                    Keep track of progress and results
                  </li>
                  <li className="">
                    Use of commit hash for full repreoducibility
                  </li>
                  <li className="">
                    Full control over computational resources
                  </li>
                </ul>
              </div>
            </div>
            <div className="demo-carousel-image border-rounded">
              <picture>
                <source srcSet="/gifs/mlreef_repository.webp" type="image/webp" />
                <img
                  src="/images/fallback/MLReef_repository.png"
                  alt="mlreef projects demo repository"
                />
              </picture>
            </div>
          </div>
        </div>
      </Slider>
    </section>
  );
};

DemoCarousel.defaultProps = {
  className: '',
};

export default DemoCarousel;
