import React from 'react';
import './InputAction.scss';

const InputAction = ({ type, buttonLabel, placeholder }) => (
  <div className="input-action input-group">
    <input
      className="form-control"
      placeholder={placeholder}
      type={type || 'text'}
    />
    <div className="input-action-append input-group-append">
      <button type="submit" className="btn btn-info px-sm-4">
        {buttonLabel || 'send'}
      </button>
    </div>
  </div>
);

export default InputAction;
