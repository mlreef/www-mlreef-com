import React, { useEffect, useState } from 'react';
import './Marketplace.scss';
import { fetchProjects } from './api';
import MarketplaceCard from './MarketplaceCard';
import HomeStatement from '../HomeStatement';
import MTabs from '../../../externals/components/MTabs';

const initialRepos = {
  projects: [],
  models: [],
  dataOps: [],
  dataVisualization: [],
};

// Helpers for update project manually
// window.fromuser = (projects, name) => projects.filter(p => p.gitlabNamespace === name);
// window.tojson = (t) => JSON.stringify(t, null, 2);
// window.findp = (projects, slug) => projects.find(p => p.slug === slug);
// window.extract = (projects, slug) => window.tojson(window.findp(projects, slug));

const Marketplace = () => {
  const [repos, setRepos] = useState(initialRepos);

  useEffect(() => {
    fetchProjects()
      // .then(res => console.log(res) || res)
      .then(setRepos);
  }, []);

  return (
    <div className="marketplace d-none d-lg-block">
      <HomeStatement
        title="A full set of tools for your ML work!"
        text="Instead of disconnected toolchains, use MLReef to run your pipelines, process your data, 
        version and track your experiments and models, manage your teams and members, ... and much more."
      />
      
    </div>
  );
};

export default Marketplace;
