import React, { useState, useEffect } from 'react';
import './MarketplaceCard.scss';
import { fetchProjectContributors } from './api';

const siteUrl = 'https://www.mlreef.com';
const iconGrey = 'images/icon_grey-01.png';

const MarketplaceCard = props => {
  const {
    projectId,
    title,
    description,
    branch,
    types,
    users,
    starCount,
    forkCount,
    namespace,
    slug,
    push,
  } = props;

  const [avatars, setAvatars] = useState([]);
  const mainDiv = React.useRef(false);

  // useEffect(() => {
  //   fetchProjectContributors(projectId)
  //     .then(async (res) => {
  //       if (!res.ok) {
  //         Promise.reject(res);
  //       } else {
  //         const contributors = await res.json();
  //         if (mainDiv.current) {
  //           setAvatars(contributors.map((cont) => ({ id: cont.id, url: cont.avatar_url })));
  //         }
  //       }
  //     });
  // }, [projectId]);

  const avatarsLength = avatars.length;

  return (
    <div
      className="card"
      ref={mainDiv}
    >
      <div className="card-container project-card-container">
        <a
          href={`${siteUrl}/${namespace}/${slug}`}
          target="_black"
          rel="noopener noreferrer"
        >
          <p className="card-title">
            {title}
          </p>

          {namespace && (
          <div
            className="project-card-container-autor pb-3 mt-2"
          >
            <p>{namespace}</p>
          </div>
          )}
          <div className="card-content">
            {types && (
              <div className="d-flex">
                {types.includes('text') && (
                  <div className="mr-2">
                    <i className="fa fa-file t-success">
                      <span className="label"> Text</span>
                    </i>
                  </div>
                )}

                {types.includes('audio') && (
                  <div className="mr-2">
                    <i className="fa fa-volume-up t-info">
                      <span className="label"> Audio</span>
                    </i>
                  </div>
                )}

                {types.includes('video') && (
                  <div className="mr-2">
                    <i className="fa fa-video t-danger">
                      <span className="label"> Video</span>
                    </i>
                  </div>
                )}

                {types.includes('tabular') && (
                  <div className="mr-2">
                    <i className="fas fa-grip-lines-vertical t-warning">
                      <span className="label"> Tabular</span>
                    </i>
                  </div>
                )}

                {types.includes('image') && (
                  <div className="mr-2">
                    <i className="fas fa-images" style={{ color: '#D2519D' }}>
                      <span className="label"> Images</span>
                    </i>
                  </div>
                )}
              </div>
            )}

            {description && description.length === 0 ? (
              <div className="d-flex noelement-found-div" style={{ marginTop: '1rem' }}>
                <img src={iconGrey} alt="" style={{ maxHeight: '30px' }} />
                <p style={{ height: 'unset' }}>No description</p>
              </div>
            ) : (
              <p className="t-dark">{description}</p>
            )}
            <div className="d-flex t-secondary">
              <div className="mr-3">
                <i className="fa fa-star">
                  <span className="label">{` ${starCount}`}</span>
                </i>
              </div>
              <div className="mr-3">
                <i className="fa fa-code-branch">
                  <span className="label">{` ${forkCount}`}</span>
                </i>
              </div>
            </div>
          </div>
        </a>
        <div className="card-actions">
          {users && (
            <div className="avatars-reversed">
              {[...users].reverse().map((ava) => (
                <a
                  key={`ava-cont-${ava.username}-${ava.id}`}
                  href={`${siteUrl}/${ava.username}`}
                  target="_black"
                  rel="noopener noreferrer"
                >
                  <div className="avatar-container">
                    <img src={ava.url} alt="" className="project-card-avatar" />
                  </div>
                </a>
              ))}
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default MarketplaceCard;
