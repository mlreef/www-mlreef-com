import Img from './Img';

export {
  Context,
  useContextValue,
  default as ImgProvider,
} from './ImgProvider';

export default Img;
