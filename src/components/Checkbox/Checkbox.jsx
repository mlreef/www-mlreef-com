import React from 'react';
import './Checkbox.scss';

const Checkbox = (props) => {
  const {
    disabled,
    name,
    label,
    className,
  } = props;

  return (
    <div
      htmlFor={`${name} ${label}`}
      className={`checkbox ${className}`}
    >
      {label && <p>{label}</p> }
      <input disabled={disabled} type="checkbox" checked />
      <span className="checkmark" />
    </div>
  );
};

export default Checkbox;
