import React from 'react';
import './ContentCardsCard.scss';

const ContentCardsCard = props => {
  const {
    title,
    color,
    blocks,
    setBlock,
    currentBlock,
  } = props;

  const mainStyles = {
    backgroundColor: color,
    color,
    borderRadius: '5px',
    borderColor: color,
    borderWidth: '2px',
  };

  const compareBlock = block => currentBlock.title === block.title;

  return (
    <div style={mainStyles} className="content-cards-card">
      <div className="content-cards-card-container">
        <div className="content-cards-card-title">{title}</div>
        <div className="content-cards-card-blocks">
          {blocks.map(block => (
            <div
              key={`block-${title}-${block.title}`}
              className="block"
            >
              <div className="block-container">
                <button
                  type="button"
                  className={`block-btn ${compareBlock(block) ? 'active' : ''}`}
                  onClick={() => setBlock(block, color)}
                >
                  {block.title}
                </button>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

ContentCardsCard.defaultProps = {
  title: '',
  color: '#888',
  blocks: [],
  setBlocks: () => {},
  currentBlock: {},
};

export default ContentCardsCard;
