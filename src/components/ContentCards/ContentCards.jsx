import React, { useState, useEffect, useRef } from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import cx from 'classnames';
import { scroller, Element as ScrollEl } from 'react-scroll';
import Img from '@components/Img';
import './ContentCards.scss';
import Card from './ContentCardsCard';

const ContentCards = props => {
  const { className } = props;

  const contRef = useRef();
  const [isContentShown, setIsContentShown] = useState(null);
  const [interColor, setInterColor] = useState('var(--secondary)');
  const [currentBlock, setCurrentBlock] = useState({
    title: 'Select a feature from above to discover what it does',
    text: 'MLReef is a holistic and open source platform with an ever expanding set of features to meet your ML requirements.',
    image: null,
  });

  const { dataYaml: { cards } } = useStaticQuery(
    graphql`
      query FetchContentCards {
        dataYaml {
          cards: contentCards {
            title
            color
            blocks {
              title
              text
              image
            }
          }
        }
      }
    `,
  );

  const scrollToContent = () => scroller.scrollTo('content', {
    offset: -66,
    smooth: true,
    duration: 300,
  });

  const scrollToCards = () => scroller.scrollTo('cards', {
    offset: -70,
    smooth: true,
    duration: 300,
  });

  const scrollCards = forward => {
    const cont = contRef.current;
    cont.scrollBy({ left: forward ? 350 : -350, behavior: 'smooth' });
  };

  const scrollToRight = () => scrollCards(true);

  const scrollToLeft = () => scrollCards(false);

  const toggleScroll = () => {
    if (!isContentShown) setIsContentShown(1);
    else setIsContentShown(0);
  };

  useEffect(
    () => {
      if (isContentShown === 1) scrollToContent();
      else if (isContentShown === 0) scrollToCards();
    },
    [isContentShown],
  );

  const handleSelectBlock = (block, color = 'var(--secondary)') => {
    setIsContentShown(1);
    setInterColor(color);
    setCurrentBlock(block);
  };

  return (
    <div className={`content-cards ${className}`}>
      <div className="content-cards-container">
        <ScrollEl name="cards">
          <div className="content-cards-cards">
            <div ref={contRef} className="content-cards-cards-container">
              {cards.map(({ title, blocks, color }) => (
                <Card
                  key={`content-card-${title}`}
                  title={title}
                  color={color}
                  blocks={blocks}
                  setBlock={handleSelectBlock}
                  currentBlock={currentBlock}
                />
              ))}
            </div>
            <button
              type="button"
              label="scroll left"
              aria-label="scroll left"
              onClick={scrollToLeft}
              className="content-cards-cards-scroll left"
            />
            <button
              type="button"
              label="scroll right"
              aria-label="scroll right"
              onClick={scrollToRight}
              className="content-cards-cards-scroll right"
            />
          </div>
        </ScrollEl>
        <ScrollEl name="content">
          <div style={{ borderColor: interColor }} className="content-cards-inter">
            <button
              type="button"
              label="scroll"
              aria-label="scroll"
              onClick={toggleScroll}
              style={{ color: interColor }}
              className={cx('btn btn-hidden fa fa-caret-down triangle', {
                inverted: isContentShown,
              })}
            />
          </div>
        </ScrollEl>
        <div className="content-cards-content">
          <div className="content-cards-content-title">
            {currentBlock.title}
          </div>
          <div className={cx({
            'content-cards-content-container': true,
            left: !!currentBlock.image,
          })}
          >
            <div className="text-content">
              {currentBlock.text && currentBlock.text.split('\n').map(line => (
                <p key={line}>{line}</p>
              ))}
            </div>
            {currentBlock.image && (
              <div className="image-content">
                <Img src={currentBlock.image} alt={currentBlock.title} />
              </div>
            )}
          </div>

        </div>

      </div>
    </div>
  );
};

ContentCards.defaultProps = {
  className: '',
};

export default ContentCards;
