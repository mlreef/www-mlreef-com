import React from 'react';
import { Link, useStaticQuery, graphql } from 'gatsby';
import './TeamGallery.scss';

const TeamGallery = props => {

  const { dataYaml: { team: { members } } } = useStaticQuery(
    graphql`
    query FetchTeamMembers {
      dataYaml {
        team {
          members {
            avatar
            href
            location
            name
            role
          }
        }
      }
    }
    `,
  );
  // console.log(members);

  return (
    <div className="team-gallery">
      <div className="team-gallery-container">
        {members.map(member => (
          <div
            key={`team-card-${member.name}`}
            className="team-gallery-card card mb-4"
          >
            <div className="team-gallery-card-container card-container">
              <div className="team-gallery-card-avatar">
                <img className="avatar" src={member.avatar} alt={member.name} />
              </div>
              <div className="team-gallery-card-content">
                <div className="name mb-2">
                  {member.name}
                </div>
                <div className="role t-info">
                  {member.role}
                </div>
                <footer className="location t-secondary">
                  {member.location}
                </footer>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default TeamGallery;
