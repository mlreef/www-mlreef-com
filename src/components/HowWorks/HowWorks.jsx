import React from 'react';
import Img from '@components/Img';
import MSimpleTabs from '@externals/MSimpleTabs';
import ProjectCard from '@components/ProjectCard';
import modelString from './model';
import './HowWorks.scss';

const projectCards = [
    {
    title: 'Identifying customer churn',
    namespace: 'erikaml',
    description: 'Extract insights to improve customer retention, calculate customers lifetime value or even predict if a client is going to leave.',
    starCount: 35,
    image: '/images/cards/churn.jpg',
    projectType: 'data',
    href: 'https://mlreef.com/erikaml/customer-churn',
  },
  {
    title: 'Painting like Monet using CycleGAN',
    namespace: 'Akane',
    description: 'The monet directories contain Monet paintings. Use these images to train your model.',
    starCount: 215,
    image: '/images/cards/monet.jpg',
    projectType: 'data',
    href: 'https://mlreef.com/Akane/painting-like-monet',
  },
  {
    title: 'Recommending Netflix movies and TV shows',
    namespace: 'erikaml',
    description: 'Create your list of movie recommendation using the Netflix catalogue using multiple methods.',
    image: '/images/cards/movies.jpg',
    starCount: 154,
    projectType: 'data',
    href: 'https://mlreef.com/erikaml/recommending-netflix-movies-and-tv-shows',
  },
  {
    title: 't-SNE',
    namespace: 'erikaml',
    description: 't-SNE is a tool to visualize high-dimensional data. It converts similarities between data points to joint probabilities and tries to minimize the Kullback-Leibler...',
    image: '/images/cards/tsne.jpg',
    starCount: 32,
    projectType: 'visualization',
    href: 'https://mlreef.com/erikaml/t-sne',
  },
];

const HowWorks = () => (
  <MSimpleTabs
    className="how-works"
    steps
    sections={[
      {
        label: 'Create or fork data & code repos',
        content: (
          <div className="d-flex">
            <div className="mx-auto w-100" style={{ maxWidth: '800px' }}>
              <div className="mx-auto t-center">

                <p className="t-dark px-3">
                  Your ML work is structured in repositories. Use ML project repos to
                  store your data, pre-process your datasets and run experiments.
                  Use own or public model, data ops and data visualization repos to
                  run code as containerized modules to speed up, structure and coordinate
                  your ML work.
                </p>
                <section className="how-works-project-cards">
                  {projectCards.map(proj => (
                    <ProjectCard
                      key={proj.title}
                      extUrl="https://mlreef.com"
                      name={proj.title}
                      description={proj.description}
                      image={proj.image}
                      stars={proj.starCount}
                      slug={proj.slug}
                      namespace={proj.namespace}
                      projectType={proj.projectType}
                      href={proj.href}
                    />
                  ))}
                </section>
                <a
                  className="btn btn-outline-info mx-auto"
                  href="https://mlreef.com/explore"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Explore all public repos!
                </a>
              </div>
            </div>
          </div>
        ),
      },
      {
        label: 'Publish to create working code',
        content: (
          <div className="d-flex">
            <div className="mx-auto w-100" style={{ maxWidth: '800px' }}>
              <div style={{ fontSize: '12px' }} className="" dangerouslySetInnerHTML={{ __html: modelString }} />
              <div className="mx-auto t-center">
                <a
                  href="https://mlreef.com/Akane/resnet50/-/blob/branch/master/path/resnet50.py"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  See the source file
                </a>
                <p className="t-dark px-3">
                  Create or upload your scripts and add parameter decorators before
                  publishing your code. The publishing will create a docker image
                  so that your code is directly accessible within an interactive UI.
                </p>
              </div>
            </div>
          </div>
        ),
      },
      {
        label: 'Run your code in built-in pipelines',
        content: (
          <div className="d-flex">
            <div style={{ maxWidth: '800px' }} className="mx-auto">
              <Img src="/images/MLReef_pipelines.png" alt="Pileline example" className="w-100" />
              <div className="mx-auto t-center">
                <p className="t-dark px-3">
                  Each published module will be available as working code for you,
                  your team or the entire MLReef community. Change its parameters and
                  execute them in the built-in pipelines.
                </p>
              </div>
            </div>
          </div>
        ),
      },
      {
        label: 'Track experiments',
        content: (
          <div className="d-flex">
            <div style={{ maxWidth: '800px' }} className="mx-auto">
              <Img src="/images/Experiment.png" alt="Experiment example" className="w-100" />
              <div className="mx-auto t-center">
                <p className="t-dark px-3">
                  Track all jobs and experiments with GIT to reproduce data, code,
                  hyperparameter values and environment settings used. Compare
                  metrics and keep track of your progress.
                </p>
              </div>
            </div>
          </div>
        ),
      },
    ]}
  />
);

export default HowWorks;
