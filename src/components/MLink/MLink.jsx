import React from 'react';
import { Link } from 'gatsby';

const MLink = props => {
  const {
    label,
    to,
    children,
    className,
    openApart,
  } = props;

  return (
    /^https?|^mailto/.test(to)
      ? (
        <a
          target={openApart ? '_blank' : null}
          href={to}
          className={className}
          rel={openApart ? 'noopener noreferrer' : null}
          label={label}
          arial-label={label}
        >
          {children}
        </a>
      ) : (
        <Link
          to={to || '/'}
          className={className}
          label={label}
          arial-label={label}
        >
          {children}
        </Link>
      )
  );
};

MLink.defaultProps = {
  className: '',
};

export default MLink;
