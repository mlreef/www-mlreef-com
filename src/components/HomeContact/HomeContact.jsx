/* eslint-disable */

// this file is not being used yet
import React from 'react';
import './HomeContact.scss';

const HomeContact = props => (
  <section className="home-contact text-white pb-0 row bg-light">
    <div className="home-contact-cover col-12 col-sm-6 bg-dark row p-5">
      <div className="col-12 text-center mt-5">
        <h2>CONTACT US</h2>
        <p>hello@mlreef.com</p>
        <div className="d-flex mx-auto justify-content-center">
          <div className="p-2"><i className="fab fa-linkedin-in"></i></div>
          <div className="p-2"><i className="fab fa-twitter"></i></div>
        </div>
      </div>
      <div className="col-12 text-center">
        <h5>impressum</h5>
      </div>
    </div>
    <div className="col-12 col-sm-6 px-2 px-sm-5 m-auto">
      <form className="px-sm-5">
        <div className="form-row">
          <div className="form-group col-12 col-sm-6">
            <input className="form-control"
              placeholder="Name" />
          </div>
          <div className="form-group col-12 col-sm-6">
            <input className="form-control"
              placeholder="Email" />
          </div>
        </div>
        <div className="form-group">
          <input className="form-control"
            placeholder="Subject" />
        </div>
        <div className="form-group">
          <textarea placeholder="Message" className="form-control" rows="5"></textarea>
        </div>
        <button className="btn btn-block btn-success">Send</button>
      </form>
    </div>
  </section>
);

export default HomeContact;
